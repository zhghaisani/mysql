﻿1. membuat database
create database myshop;

2. membuat tabel
- tabel categories
create table categories(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
-tabel items
 create table items(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int (10),
    -> stock int (10),
    -> category_id int(10),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );
-tabel users
 create table users(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. Memasukkan data ke tabel
- categories
insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");
-items
 insert into items (name, description, price, stock, category_id)
    -> values
    -> ('sumsang b50','hape keren dari merek sumsang','4000000','100',1),
    -> ('Uniklooh','baju keren dari brand ternama','500000','50',2),
    -> ('IMHO Watch',' jam tangan anak yang jujur banget','2000000','10',1);
-users
insert into users(name, email, password) values("John Doe", "john@doe
.com","john123"),("Jane Doe","jane@doe.com","jenita123");

4. a.  select name, email from users;
b1. select * from items where price >= 1000000;
b2. select * from items where name like "%sang%";

5.  select items.id, items.name, items.description, items.price, items.stock, it
ems.category_id, categories.name from items inner join categories on items.category_id = categ
ories.id;

 update items set price = "2500000" where id = 1;